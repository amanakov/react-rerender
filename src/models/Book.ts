export interface Book {
  id: number;
  price: number;
}

export function mockBook() {
  return {
    id: Math.round(Math.random() * 1_000_000),
    price: Math.round(Math.random() * 10),
  };
}

export function mockBooks(count = 5) {
  return Array(count)
    .fill(1)
    .map(() => mockBook());
}
