import { FC } from 'react';
import { RootState } from '../../store';
import { Block } from '../Block';
import { useSelector, useDispatch } from 'react-redux';
import { increment2 } from '../../slices/countersSlice';

interface Props {}

export const Counter2Block: FC<Props> = () => {
  console.log('Counter2Block: render');
  const counter2 = useSelector(
    (state: RootState) => {
      console.log('Counter2Block: selector', state);
      return state.counters.counter2;
    },
    // (counterLeft, counterRight) => {
    //   console.log('Counter2Block: equalityFn', { counterLeft, counterRight });
    //   return counterLeft === counterRight;
    // }
  );
  const dispatch = useDispatch();
  return (
    <Block title="Counter 2">
      <p>const counter2 = useSelector((state: RootState) =&gt; state.counters.counter2);</p>
      <p>
        counter2: {counter2}
        <button onClick={() => dispatch(increment2())}>+</button>
      </p>
    </Block>
  );
};
