import { FC } from 'react';
import { Block } from '../Block';
import { useDispatch } from 'react-redux';
import { increment2 } from '../../slices/countersSlice';

interface Props {}

export const DispatchBlock: FC<Props> = () => {
  const dispatch = useDispatch();
  return (
    <Block title="Dispatch">
      <p>Просто dispatch без подписки, поэтому необновляется</p>
      <p>
        counter2: ХЗ
        <button onClick={() => dispatch(increment2())}>+</button>
      </p>
    </Block>
  );
};
