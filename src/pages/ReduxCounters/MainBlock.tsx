import { FC } from 'react';
import { Block } from '../Block';
import { Counter1Block } from './Counter1Block';
import { Counter2Block } from './Counter2Block';
import { CountersBlock } from './CountersBlock';
import { DispatchBlock } from './DispatchBlock';

interface Props {}

export const MainBlock: FC<Props> = () => {
  return (
    <Block title="Main block">
      <Counter1Block />
      <Counter2Block />
      <CountersBlock />
      <DispatchBlock />
    </Block>
  );
};
