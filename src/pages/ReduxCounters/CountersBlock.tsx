import { FC } from 'react';
import { RootState } from '../../store';
import { Block } from '../Block';
import { useSelector, useDispatch } from 'react-redux';
import { increment2 } from '../../slices/countersSlice';

interface Props {}

export const CountersBlock: FC<Props> = () => {
  console.log('CountersBlock: render');
  const counters = useSelector(
    (state: RootState) => {
      console.log('CountersBlock: selector', state);
      return state.counters;
    }
    // (countersLeft, countersRight) => {
    //   console.log('CountersBlock: equalityFn', { countersLeft, countersRight });
    //   return countersLeft === countersRight;
    // }
  );
  const counter2 = counters.counter2;
  const dispatch = useDispatch();
  return (
    <Block title="Counters">
      <p>Подписка на все counters поэтому обновзяется всегда, когда обновляется counters</p>
      <p>const counters = useSelector((state: RootState) =&gt; state.counters);</p>
      <p>const counter2 = counters.counter2;</p>
      <p>
        counter2: {counter2}
        <button onClick={() => dispatch(increment2())}>+</button>
      </p>
    </Block>
  );
};
