import { FC } from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';

interface Props {}

export const ReduxCountersPage: FC<Props> = () => {
  return (
    <>
      <Header title="Redux counters" />
      <p>
        После изменения state в redux store обновляются только те эл-ты, у которых поменялась возвращаемое значение в
        useSelector
      </p>
      <MainBlock />
    </>
  );
};
