import { FC } from 'react';
import { RootState } from '../../store';
import { Block } from '../Block';
import { useSelector, useDispatch } from 'react-redux';
import { increment1 } from '../../slices/countersSlice';

interface Props {}

export const Counter1Block: FC<Props> = () => {
  console.log('Counter1Block: render');
  const counter1 = useSelector(
    (state: RootState) => {
      console.log('Counter1Block: selector', state);
      return state.counters.counter1;
    },
    // (counterLeft, counterRight) => {
    //   console.log('Counter1Block: equalityFn', { counterLeft, counterRight });
    //   return counterLeft === counterRight;
    // }
  );
  const dispatch = useDispatch();
  return (
    <Block title="Counter 1">
      <p>const counter1 = useSelector((state: RootState) =&gt; state.counters.counter1);</p>
      <p>
        counter1: {counter1}
        <button onClick={() => dispatch(increment1())}>+</button>
      </p>
    </Block>
  );
};
