import { FC } from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';

interface Props {}

export const ReselectPage: FC<Props> = () => {
  return (
    <>
      <Header title="Reselect" />
      <p>
        Селекторы, созданые через createSelector, вызываются если только поменялись зависимости. Иначе возвращают
        предыдущее значение.
      </p>
      <MainBlock />
    </>
  );
};
