import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Block } from '../Block';

import { booksSelector, booksInProgressSelector, booksTotalPriceSelector } from '../../selectors/booksSelector';
import { completeRequest, startRequest } from '../../slices/booksSlice';
import { mockBooks } from '../../models/Book';
import { setBooks } from '../../slices/entities/booksSlice';

interface Props {}

export const MainBlock: FC<Props> = () => {
  const books = useSelector(booksSelector);
  const booksTotalPrice = useSelector(booksTotalPriceSelector);
  const inProgress = useSelector(booksInProgressSelector);

  const dispatch = useDispatch();
  const fetchBooks = () => {
    dispatch(startRequest());
    setTimeout(() => {
      const books = mockBooks();
      const bookEntities = books.reduce((entities, book) => ({ ...entities, [book.id]: book }), {});
      const bookIds = books.map((book) => book.id);
      dispatch(setBooks(bookEntities));
      dispatch(completeRequest(bookIds));
    }, 2 * 1000);
  };

  useEffect(() => {
    fetchBooks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Block title="Main block">
      <p>
        <button onClick={fetchBooks}>Fetch books</button>
      </p>
      {inProgress && <p>In progress..</p>}
      {books.map((book) => (
        <Block title={`Book: ${book.id}`} key={book.id}>
          <p>Price: ${book.price}</p>
        </Block>
      ))}
      {books.length > 0 && <p>Total price: ${booksTotalPrice}</p>}
    </Block>
  );
};
