import { FC } from 'react';
import { Block } from '../Block';
import { MainBlock2 } from './MainBlock2';

interface Props {}

export const MainBlock: FC<Props> = () => {
  return (
    <Block title="Main block">
      <MainBlock2 />
    </Block>
  );
};
