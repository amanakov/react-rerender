import { FC, useContext } from 'react';
import { Block } from '../Block';
import { context } from './Provider';
import { SimpleBlock } from './SimpleBlock';

interface Props {}

export const Consumer: FC<Props> = () => {
  const { count, setCount } = useContext(context);
  return (
    <Block title="consumer">
      <p>
        count: {count} <button onClick={() => setCount(count + 1)}>+</button>
      </p>
      <SimpleBlock />
    </Block>
  );
};
