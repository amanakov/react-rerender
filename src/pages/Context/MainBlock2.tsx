import { FC } from 'react';
import { Block } from '../Block';
import { MainBlock3 } from './MainBlock3';

interface Props {}

export const MainBlock2: FC<Props> = () => {
  return (
    <Block title="Main block 2">
      <MainBlock3 />
    </Block>
  );
};
