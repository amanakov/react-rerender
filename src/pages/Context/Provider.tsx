import { FC, createContext, useState, useMemo } from 'react';

interface Props {}

interface ContextValue {
  count: number;
  setCount: (i: number) => void;
}

export const context = createContext<ContextValue>({ count: 0, setCount: () => {} });

export const Provider: FC<Props> = ({ children }) => {
  const [count, setCount] = useState(0);
  const value = useMemo(() => ({ count, setCount }), [count, setCount]);
  return <context.Provider value={value}>{children}</context.Provider>;
};
