import { FC } from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';
import { Provider } from './Provider';

interface Props {}

export const Context: FC<Props> = () => {
  return (
    <>
      <Header title="context" />
      <p>
        Контекст: изменение в value контекста вызывает перерендер только тех компонент (и их детей), которые подписаны
        на него
      </p>
      <Provider>
        <MainBlock />
      </Provider>
    </>
  );
};
