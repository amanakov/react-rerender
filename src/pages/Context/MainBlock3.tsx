import { FC } from 'react';
import { Block } from '../Block';
import { Consumer } from './Consumer';

interface Props {}

export const MainBlock3: FC<Props> = () => {
  return (
    <Block title="Main block 3">
      <Consumer />
    </Block>
  );
};
