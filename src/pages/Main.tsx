import React from 'react';
import { Link } from 'react-router-dom';

interface Props {}

export const Main: React.FC<Props> = () => {
  return (
    <div>
      <ol>
        <li>
          <Link to="/state-rerender">State rerender</Link>
        </li>
        <li>
          <Link to="/memo">memo</Link>
        </li>
        <li>
          <Link to="/context">context</Link>
        </li>
        <li>
          <Link to="/hoc">HOC</Link>
        </li>
        <li>
          <Link to="/render-props">RenderProps</Link>
        </li>
        <li>
          <Link to="/redux-counters">Redux counters</Link>
        </li>
        <li>
          <Link to="/reselect">Reselect</Link>
        </li>
      </ol>
    </div>
  );
};
