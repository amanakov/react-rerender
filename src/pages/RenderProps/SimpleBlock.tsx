import { FC } from 'react';
import { Block } from '../Block';

interface Props {}

export const SimpleBlock: FC<Props> = () => {
  return <Block title="Simple block">Просто блок</Block>;
};
