import { FC, useState } from 'react';
import { Block } from '../Block';
import { RenderProps } from './RenderProps';
import { SimpleBlock } from './SimpleBlock';

interface Props {}

export const MainBlock: FC<Props> = () => {
  const [count, setCount] = useState(0);
  return (
    <Block title="Main block">
      <p>
        count: {count} <button onClick={() => setCount(count + 1)}>+</button>
      </p>
      <RenderProps>{() => <SimpleBlock />}</RenderProps>
    </Block>
  );
};
