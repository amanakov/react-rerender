import { FC, useState } from 'react';
import { Block } from '../Block';

interface Props {
  children: any;
}

export const RenderProps: FC<Props> = ({ children }) => {
  const [count, setCount] = useState(0);
  return (
    <Block title="Render props">
      <p>
        count: {count} <button onClick={() => setCount(count + 1)}>+</button>
      </p>
      {children(count, setCount)}
    </Block>
  );
};
