import { FC } from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';

interface Props {}

export const RenderPropsPage: FC<Props> = () => {
  return (
    <>
      <Header title="Render props" />
      <p>Вызывает ли перерендер RenderProps-a перерендер children компонент? Да, вызывает.</p>
      <MainBlock />
    </>
  );
};
