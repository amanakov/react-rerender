import { FC, HTMLAttributes, useEffect, useRef } from 'react';
import classNames from 'classnames';
import styles from './Block.module.css';

interface Props extends HTMLAttributes<HTMLDivElement> {
  title: string;
}

export const Block: FC<Props> = ({ title, children, className, ...props }) => {
  const ref = useRef<HTMLParagraphElement | null>(null);

  useEffect(() => {
    const rerenderClass = styles['block__title_rerendered'];
    if (ref.current) {
      ref.current.classList.add(rerenderClass);
      setTimeout(() => {
        ref.current?.classList.remove(rerenderClass);
      }, 300);
    }
  });

  return (
    <div className={classNames(styles['block'], className)} {...props}>
      <p className={styles['block__title']} ref={ref}>
        {title}
      </p>
      {children}
    </div>
  );
};
