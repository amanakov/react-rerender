import { FC } from 'react';
import { Link } from 'react-router-dom';
import styles from './Header.module.css';

interface Props {
  title: string;
}

export const Header: FC<Props> = ({ title }) => {
  return (
    <div className={styles['header']}>
      <Link to="/" className={styles['header__back']}>
        На главную
      </Link>
      <h1 className={styles['header__title']}>{title}</h1>
    </div>
  );
};
