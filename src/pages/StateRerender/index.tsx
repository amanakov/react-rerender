import React from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';

interface Props {}

export const StateRerender: React.FC<Props> = () => {
  return (
    <>
      <Header title="State rerender" />
      <p>Компонента ререндерится либо когда меняется её state либо когда ререндерится её parent</p>
      <MainBlock />
    </>
  );
};
