import { FC, useState } from 'react';
import { Block } from '../Block';
import { SimpleBlock } from './SimpleBlock';
import { StatefulBlock } from './StatefulBlock';

interface Props {}

export const MainBlock: FC<Props> = () => {
  const [count, setCount] = useState(0);
  return (
    <Block title="Main block">
      <p>
        count: {count} <button onClick={() => setCount(count + 1)}>+</button>
      </p>
      <SimpleBlock />
      <StatefulBlock />
    </Block>
  );
};
