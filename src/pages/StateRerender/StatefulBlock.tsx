import { FC, useState } from 'react';
import { Block } from '../Block';
import { SimpleBlock } from './SimpleBlock';

interface Props {}

export const StatefulBlock: FC<Props> = () => {
  const [count, setCount] = useState(0);
  return (
    <Block title="Stateful block">
      <p>
        count: {count} <button onClick={() => setCount(count + 1)}>+</button>
      </p>
      <SimpleBlock />
      <SimpleBlock />
    </Block>
  );
};
