import { FC, useState } from 'react';
import { Block } from '../Block';

interface Props {}

export const HOC: FC<Props> = ({ children }) => {
  const [count, setCount] = useState(0);
  return (
    <Block title="HOC">
      <p>
        count: {count} <button onClick={() => setCount(count + 1)}>+</button>
      </p>
      {children}
    </Block>
  );
};
