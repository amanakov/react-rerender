import { FC } from 'react';
import { Block } from '../Block';
import { HOC } from './HOC';
import { SimpleBlock } from './SimpleBlock';

interface Props {}

export const MainBlock: FC<Props> = () => {
  return (
    <Block title="Main block">
      <HOC>
        <SimpleBlock />
      </HOC>
    </Block>
  );
};
