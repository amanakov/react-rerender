import { FC } from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';

interface Props {}

export const HOCPage: FC<Props> = () => {
  return (
    <>
      <Header title="HOC" />
      <p>Вызывает ли перерендер HOC-a перерендер children компонент? Нет, не вызывает.</p>
      <MainBlock />
    </>
  );
};
