import { FC, memo } from 'react';
import { Block } from '../Block';

interface Props {}

const _MemoBlock: FC<Props> = () => {
  return (
    <Block title="Memo block">
      <p>Блок с memo</p>
    </Block>
  );
};

export const MemoBlock = memo(_MemoBlock);
