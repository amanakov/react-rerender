import { FC } from 'react';
import { Header } from '../Header';
import { MainBlock } from './MainBlock';

interface Props {}

export const Memo: FC<Props> = () => {
  return (
    <>
      <Header title="memo" />
      <p>React.memo (не useMemo) помогает избежать ререндера из-за родителя, если props не изменились</p>
      <MainBlock />
    </>
  );
};
