import { FC, useState } from 'react';
import { Block } from '../Block';

import { SimpleBlock } from './SimpleBlock';
import { MemoBlock } from './MemoBlock';
import { CountMemoBlock } from './CountMemoBlock';

interface Props {}

export const MainBlock: FC<Props> = () => {
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);

  return (
    <Block title="Main block">
      <p>
        count 1: {count1} <button onClick={() => setCount1(count1 + 1)}>+</button>
      </p>
      <p>
        count 2: {count2} <button onClick={() => setCount2(count2 + 1)}>+</button>
      </p>
      <SimpleBlock />
      <MemoBlock />
      <CountMemoBlock mainBlockCount={count1} />
    </Block>
  );
};
