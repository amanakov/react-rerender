import { FC, memo } from 'react';
import { Block } from '../Block';

interface Props {
  mainBlockCount: number;
}

const _CountMemoBlock: FC<Props> = ({ mainBlockCount }) => {
  return (
    <Block title="Memo block с зависимостю от count 1">
      <p>Блок с memo но с props зависящей от count 1 в main</p>
      <p>Count 1: {mainBlockCount}</p>
    </Block>
  );
};

export const CountMemoBlock = memo(_CountMemoBlock);
