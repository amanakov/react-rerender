import { combineReducers } from '@reduxjs/toolkit';

import entitiesReducer from './entities';
import booksReducer from './booksSlice';
import countersReducer from './countersSlice';

export default combineReducers({
  entities: entitiesReducer,
  books: booksReducer,
  counters: countersReducer,
});
