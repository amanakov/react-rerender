import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface CounterState {
  booksIds: number[];
  inProgress: boolean;
}

const initialState: CounterState = {
  booksIds: [],
  inProgress: false,
};

export const booksSlice = createSlice({
  name: 'books',
  initialState,
  reducers: {
    startRequest: (state) => ({ ...state, inProgress: true }),
    completeRequest: (state, action: PayloadAction<number[]>) => ({
      ...state,
      inProgress: false,
      booksIds: action.payload,
    }),
  },
});

// Action creators are generated for each case reducer function
export const { startRequest, completeRequest } = booksSlice.actions;

export default booksSlice.reducer;
