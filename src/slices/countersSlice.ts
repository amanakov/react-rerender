import { createSlice } from '@reduxjs/toolkit';

export interface CounterState {
  counter1: number;
  counter2: number;
}

const initialState: CounterState = {
  counter1: 0,
  counter2: 0,
};

export const counterSlice = createSlice({
  name: 'counters',
  initialState,
  reducers: {
    increment1: (state) => ({ ...state, counter1: state.counter1 + 1 }),
    increment2: (state) => ({ ...state, counter2: state.counter2 + 1 }),
  },
});

// Action creators are generated for each case reducer function
export const { increment1, increment2 } = counterSlice.actions;

export default counterSlice.reducer;
