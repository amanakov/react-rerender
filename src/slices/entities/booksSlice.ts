import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Book } from '../../models/Book';

export interface BooksEntitiesState {
  [bookId: number]: Book;
}

const initialState: BooksEntitiesState = {};

export const booksSlice = createSlice({
  name: 'books',
  initialState,
  reducers: {
    setBook: (state, action: PayloadAction<Book>) => ({ ...state, [action.payload.id]: action.payload }),
    setBooks: (state, action: PayloadAction<BooksEntitiesState>) => ({ ...state, ...action.payload }),
  },
});

// Action creators are generated for each case reducer function
export const { setBook, setBooks } = booksSlice.actions;

export default booksSlice.reducer;
