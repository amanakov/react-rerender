import { combineReducers } from '@reduxjs/toolkit';
import booksReducer from './booksSlice';

export default combineReducers({
  books: booksReducer,
});
