import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { store } from './store';
import { Provider } from 'react-redux';

import { Main } from './pages/Main';
import { StateRerender } from './pages/StateRerender';
import { Memo } from './pages/Memo';
import { Context } from './pages/Context';
import { HOCPage } from './pages/HOC';
import { RenderPropsPage } from './pages/RenderProps';
import { ReduxCountersPage } from './pages/ReduxCounters';
import { ReselectPage } from './pages/Reselect';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/state-rerender" element={<StateRerender />} />
          <Route path="/memo" element={<Memo />} />
          <Route path="/context" element={<Context />} />
          <Route path="/hoc" element={<HOCPage />} />
          <Route path="/render-props" element={<RenderPropsPage />} />
          <Route path="/redux-counters" element={<ReduxCountersPage />} />
          <Route path="/reselect" element={<ReselectPage />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
