import { createSelector } from 'reselect';
import { entitiesSelector } from '../entitiesSelector';

export const booksEntitiesSelector = createSelector(entitiesSelector, (state) => state.books);
