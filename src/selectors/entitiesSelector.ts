import { RootState } from '../store';

export const entitiesSelector = (store: RootState) => store.entities;
