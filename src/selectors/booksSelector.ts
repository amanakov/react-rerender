import { createSelector } from 'reselect';
import { RootState } from '../store';
import { booksEntitiesSelector } from './entities/booksSelector';

const booksStateSelector = (store: RootState) => store.books;

const booksIdsSelector = createSelector(booksStateSelector, (booksState) => booksState.booksIds);

export const booksSelector = createSelector(booksIdsSelector, booksEntitiesSelector, (booksIds, booksEntities) => {
  console.log('booksSelector');
  return booksIds.map((bookId) => {
    const book = booksEntities[bookId];
    if (!book) {
      throw new Error(`Could not find book with id: ${bookId}`);
    }
    return book;
  });
});

export const booksTotalPriceSelector = createSelector(booksSelector, (books) =>
  books.reduce((totalPrice, book) => totalPrice + book.price, 0)
);

export const booksInProgressSelector = createSelector(booksStateSelector, (booksState) => booksState.inProgress);
